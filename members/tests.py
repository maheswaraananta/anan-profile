from django.test import TestCase

from django.test import TestCase, Client

# Create your tests here.
class Test(TestCase):
    #Register
    def test_url_register_exist(self):
        response = Client().get('/members/register/')
        self.assertEquals(response.status_code, 200)

    def test_complete_register(self):
        response = Client().get('/members/register/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Register", html_kembalian)
        self.assertIn("Username", html_kembalian)
        self.assertIn("Password", html_kembalian)

    def test_template_register_exist(self):
        response = Client().get('/members/register/')
        self.assertTemplateUsed(response, 'registration/register.html')

