from django.test import TestCase, Client
from django.urls import reverse, resolve


class TestStory7(TestCase):

  def test_apakah_url_story7_ada(self):
    response = Client().get('/story7/')
    self.assertEquals(response.status_code, 200)

  def test_apakah__accordion_ada_Accordion(self):
    response = Client().get('/story7/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Search Items", html_kembalian)

  def test_apakah__accordion_ada_Organisasi(self):
    response = Client().get('/story7/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Organisasi/Panitia", html_kembalian)

  def test_apakah__accordion_ada_Prestasi(self):
    response = Client().get('/story7/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Prestasi", html_kembalian)

  def test_apakah__accordion_ada_Goals(self):
    response = Client().get('/story7/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Goals", html_kembalian)

  def test_apakah_accordion_ada_searchbox(self):
    response = Client().get('/story7/data/?search=spongebob')
    self.assertEquals(response.status_code, 200)
