from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from .forms import MatkulForm
from .models import Matkul
from django.urls import reverse_lazy

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def about(request):
    return render(request, 'home/about.html')

def experience(request):
    return render(request, 'home/experience.html')

def contact(request):
    return render(request, 'home/contact.html')

class ScheduleView(ListView):
    model = Matkul
    template_name = 'home/schedule.html'
class ScheduleDetail(DetailView):
    model = Matkul
    template_name = 'home/scheduledetails.html'
class AddMatkul(CreateView):
    model = Matkul
    template_name = 'home/addschedule.html'
    fields = '__all__'
class DeleteMatkul(DeleteView):
    model = Matkul
    template_name = 'home/deletematkul.html'
    success_url = reverse_lazy('home:schedule')

