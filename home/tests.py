from django.test import TestCase

from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Matkul

# Create your tests here.
class Testing123(TestCase):
  def setUp(self):
    self.client = Client()
    self.test_subject = Matkul.objects.create(
      matkul = "PePeW",
      dosen = "olaf",
      sks = "3",
      tahun = "2020",
      deskripsi = "Calon web dev",
      ruang = "groupthink"
    )

  def test_activity_POST(self):
    self.assertEqual(str(self.test_subject.matkul), "PePeW")

  def test_activity_get_absolute_url(self):
    self.assertEqual('/schedule', self.test_subject.get_absolute_url())

  def test_ada_index(self):
    response = Client().get('/index')
    self.assertEquals(response.status_code, 200)
  
  def test_ada_about(self):
    response = Client().get('/about')
    self.assertEquals(response.status_code, 200)
  
  def test_ada_experience(self):
    response = Client().get('/experience')
    self.assertEquals(response.status_code, 200)
  
  def test_ada_contact(self):
    response = Client().get('/contact')
    self.assertEquals(response.status_code, 200)
  
  def test_ada_schedule(self):
    response = Client().get('/schedule')
    self.assertEquals(response.status_code, 200)
  
  def test_ada_addmatkul(self):
    response = Client().get('/add-matkul')
    self.assertEquals(response.status_code, 200)
  
  def test_ada_scheduledetail(self):
    response = Client().get('/schedule-detail/1')
    self.assertEquals(response.status_code, 200)
  
  def test_ada_delete(self):
    response = Client().get('/detail/1/remove')
    self.assertEquals(response.status_code, 200)

  

  




