from django.db import models
from django.urls import reverse

# Create your models here.
class Matkul(models.Model):
    matkul = models.CharField(max_length=30, default='')
    dosen = models.CharField(max_length=60, default='')
    sks = models.CharField(max_length=10, default='')
    tahun = models.CharField(max_length=30, default='')
    deskripsi = models.CharField(max_length=300, default='')
    ruang = models.CharField(max_length=10, default='')
    
    def get_absolute_url(self):
        return reverse("home:schedule")


