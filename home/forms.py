from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'matkul',
            'dosen',
            'sks',
            'tahun',
            'deskripsi',
            'ruang'
        ]
        