from django.urls import path
from . import views
from .views import ScheduleView, ScheduleDetail, AddMatkul, DeleteMatkul

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('about', views.about, name='about'),
    path('experience', views.experience, name='experience'),
    path('contact', views.contact, name='contact'),
    path('schedule', ScheduleView.as_view(), name='schedule'),
    path('schedule-detail/<int:pk>', ScheduleDetail.as_view(),name= 'schedule-detail'),
    path('add-matkul', AddMatkul.as_view(), name='add-matkul'),
    path('detail/<int:pk>/remove', DeleteMatkul.as_view(), name='delete'),
]