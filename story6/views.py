from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from .models import BukuTamu, Namapeserta

class Buku_Tamu(ListView):
  context_object_name = 'list'
  template_name = 'bukutamu.html'
  queryset = BukuTamu.objects.all()

  def get_context_data(self, **kwargs):
    context = super(Buku_Tamu, self).get_context_data(**kwargs)
    context['nama_peserta'] = Namapeserta.objects.all()
    return context

class AddNama(CreateView):
    model = Namapeserta
    template_name = 'tambahtamu.html'
    fields = '__all__'

class AddAcara(CreateView):
    model = BukuTamu
    template_name = 'tambahacara.html'
    fields = '__all__'
