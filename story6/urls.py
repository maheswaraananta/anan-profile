from django.urls import path
from . import views
from .views import Buku_Tamu, AddAcara, AddNama

app_name = 'story6'

urlpatterns = [
    path('bukutamu', Buku_Tamu.as_view(), name='bukutamu'),
    path('tambah-tamu', AddNama.as_view(), name='tambah-tamu'),
    path('tambah-acara', AddAcara.as_view(), name='tambah-acara')

]