from django.db import models
from django.urls import reverse

class BukuTamu(models.Model):
    event = models.CharField(max_length=30, default='')

    def __str__(self):
        return self.event
    
    def get_absolute_url(self):
        return reverse('story6:bukutamu')

class Namapeserta(models.Model):
    nama = models.CharField(max_length=30, default='')
    BukuTamu = models.ForeignKey(BukuTamu, null=True, on_delete= models.CASCADE)

    def __str__(self):
        return self.nama
    
    def get_absolute_url(self):
        return reverse('story6:bukutamu')
