from django import forms
from .models import Namapeserta, BukuTamu

class AcaraForm(forms.ModelForm):
    class Meta:
        model = BukuTamu
        fields = '__all__'
        widgets = {
            'activity' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "What do you want to do ?"}),
        }
class NamaForm(forms.ModelForm):
    class Meta:
        model = Namapeserta
        fields = '__all__'
        widgets = {
            'activity' : forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': "What is your name?"}),
        }
