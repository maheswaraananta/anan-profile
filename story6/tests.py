from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import BukuTamu, Namapeserta

# Create your tests here.
class Testing123(TestCase):
  def setUp(self):
    self.client = Client()
    self.test_activity = BukuTamu.objects.create(
      event = "ngoding"
    )
    self.test_peserta = Namapeserta.objects.create(
      nama = "anan",
      BukuTamu = self.test_activity
    )

  def test_activity_POST(self):
    self.assertEqual(str(self.test_activity.event), "ngoding")

  def test_activity_get_absolute_url(self):
    self.assertEqual('/story6/bukutamu', self.test_activity.get_absolute_url())
  
  def test_peserta_POST(self):
    self.assertEqual(str(self.test_peserta.nama), "anan")

  def test_peserta_get_absolute_url(self):
    self.assertEqual('/story6/bukutamu', self.test_peserta.get_absolute_url())

  def test_apakah_url_bukutamu_ada(self):
    response = Client().get('/story6/bukutamu')
    self.assertEquals(response.status_code, 200)

  def test_apakah__Buku_Tamu_ada_Judul(self):
    response = Client().get('/story6/bukutamu')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Buku Tamu", html_kembalian)

  def test_apakah_di_halaman_formulir_ada_templatenya(self):
    response = Client().get('/story6/bukutamu')
    self.assertTemplateUsed(response, 'bukutamu.html')

  def test_apakah_url_tambahacara_ada(self):
    response = Client().get('/story6/tambah-acara')
    self.assertEquals(response.status_code, 200)

  def test_apakah_di_halaman_hasil_ada_templatenya(self):
    response = Client().get('/story6/tambah-acara')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("Tambahkan Acara", html_kembalian)
    self.assertTemplateUsed(response, 'tambahacara.html')

  def test_apakah_sudah_ada_model_bukutamu(self):
    BukuTamu.objects.create(event="INI NAMA")
    hitung_berapa_bukunya = BukuTamu.objects.all().count()
    self.assertEquals(hitung_berapa_bukunya, 2)
	
  
  



